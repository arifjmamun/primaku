import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';

import { appConfig } from '../../common/config/app.config';
import { TokenPayload } from '../interfaces';
import { ContextUser } from './../../users/interfaces';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor() {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: appConfig.jwtAccessTokenSecret,
		} as StrategyOptions);
	}

	async validate(payload: TokenPayload): Promise<ContextUser> {
		return {
			id: payload.sub,
			role: payload.role,
			email: payload.sub
		};
	}
}
