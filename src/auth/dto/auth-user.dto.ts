import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { IsString, IsNotEmpty } from 'class-validator';

export class AuthUserDto {
	@ApiProperty()
	@Expose()
	@IsNotEmpty()
	@IsString()
	token: string;
}
