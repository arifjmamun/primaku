import { ApiProperty } from '@nestjs/swagger';
import { Expose, Transform } from 'class-transformer';
import { IsNotEmpty, IsEmail, IsString, MaxLength, MinLength } from 'class-validator';

export class LoginDto {
	@ApiProperty({ required: true })
	@IsNotEmpty()
	@IsEmail()
	@Transform(({ value }) => value.toLowerCase().trim())
	@Expose()
	email: string;

	@ApiProperty({ required: true })
	@IsString()
	@MinLength(8)
	@MaxLength(32)
	@Expose()
	password: string;
}
