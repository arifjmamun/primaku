import { ApiProperty, PickType } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { IsNotEmpty, IsString, IsBoolean } from 'class-validator';

import { Password } from '@app/utils/validators';
import { PASSWORD_VALIDATOR_OPTION } from '../../common/constants';
import { User } from '../../users/models/user.model';

export class ChangePasswordDto extends PickType(User, ['email']) {
	@ApiProperty({ required: true })
	@IsNotEmpty()
	@IsString()
	@Expose()
	oldPassword: string;

	@ApiProperty({ required: true })
	@Password(PASSWORD_VALIDATOR_OPTION)
	@Expose()
	newPassword: string;
}

export class PasswordChangedDto {
	@ApiProperty({ required: true })
	@IsBoolean()
	@Expose()
	isPasswordChanged: boolean;
}
