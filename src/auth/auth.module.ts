import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { appConfig } from '../common/config/app.config';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';

@Module({
	imports: [
		PassportModule,
		JwtModule.register({
			secret: appConfig.jwtAccessTokenSecret,
			signOptions: {
				expiresIn: appConfig.jwtAccessTokenExpiresIn,
			},
		}),
	],
	providers: [
		AuthService,
		LocalStrategy,
		JwtStrategy
	],
	exports: [AuthService],
	controllers: [AuthController],
})
export class AuthModule {}
