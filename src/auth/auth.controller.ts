import { Body, Controller, HttpCode, Post, Put, UseGuards } from '@nestjs/common';
import { ApiTags, ApiBody, ApiBearerAuth } from '@nestjs/swagger';

import { ApiOkResponse, Serialize, User } from '@app/utils';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { ContextUser } from '../users/interfaces';
import {
	AuthUserDto,
	LoginDto,
	ChangePasswordDto
} from './dto';
import { PasswordChangedDto } from './dto/change-password.dto';
import { AuthService } from './auth.service';
import { Role } from '../common';
import { Roles } from './decorators/roles.decorator';

@ApiTags('Auth')
@Serialize()
@Controller()
export class AuthController {
	constructor(private readonly authService: AuthService) {}

	@ApiBearerAuth()
	@ApiOkResponse({ type: PasswordChangedDto })
	@Roles(Role.Admin, Role.User)
	@UseGuards(JwtAuthGuard)
	@Put('password')
	async changePassword(@Body() dto: ChangePasswordDto, @User() user: ContextUser) {
		return this.authService.changePassword(user.id, dto);
	}

	@ApiOkResponse({ type: AuthUserDto, status: 200 })
	@ApiBody({ type: LoginDto })
	@HttpCode(200)
	@UseGuards(LocalAuthGuard)
	@Post('login')
	async login(@User() user: AuthUserDto) {
		return user;
	}
}
