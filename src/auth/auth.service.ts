import { Injectable } from '@nestjs/common';
import { JwtService, JwtSignOptions } from '@nestjs/jwt';
import { plainToInstance } from 'class-transformer';

import {
	UserNotFoundByEmailException,
	UserInvalidCredentialException,
	UserInvalidEmailAddressException,
	UserInvalidPasswordException,
	UserDeactivatedException,
} from './../users/users.exceptions';

import { AuthUserDto, ChangePasswordDto, PasswordChangedDto } from './dto';
import { UsersService } from '../users/users.service';
import { Hashing } from '@app/utils';
import { TokenPayload } from './interfaces';
import { User } from '../users/models/user.model';
import { appConfig, classTransformOptions, Role } from '../common';

@Injectable()
export class AuthService {
	constructor(private readonly usersService: UsersService, private readonly jwtService: JwtService) {}

	public async changePassword(email: string, dto: ChangePasswordDto) {
		if (email !== dto.email) throw new UserInvalidEmailAddressException();

		const user = await this.usersService.getRawUser(email);
		if (!Hashing.verifyPassword(dto.oldPassword, user.password)) throw new UserInvalidPasswordException();

		const hashedPassword = Hashing.hashPassword(dto.newPassword);
		await this.usersService.updatePassword(user.id, hashedPassword);

		return plainToInstance(PasswordChangedDto, { isPasswordChanged: true });
	}

	public async login(email: string, password: string) {
		const user = await this.usersService.getRawUser(email);
		if (!user) throw new UserNotFoundByEmailException(email);
		else if (!user.isActive) throw new UserDeactivatedException();

		if (!Hashing.verifyPassword(password, user.password)) throw new UserInvalidCredentialException();

		const token = await this.generateJwtToken(user);
		return plainToInstance(AuthUserDto, { token }, classTransformOptions);
	}

	private async generateJwtToken(user: User) {
		const tokenPayload: TokenPayload = {
			id: user.id,
			sub: user.email,
			role: user.role as Role,
		};
		const options: JwtSignOptions = { expiresIn: appConfig.jwtAccessTokenExpiresIn };
		return await this.jwtService.signAsync(tokenPayload, options);
	}
}
