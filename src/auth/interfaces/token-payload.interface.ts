import { Role } from '../../common';

export interface TokenPayload {
	id: number;
	sub: string;
	role: Role;
}
