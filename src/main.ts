import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';

import { GlobalExceptionFilter, GlobalResponseTransformer } from '@app/utils';
import { AppModule } from './app.module';
import { appConfig, configureSwagger } from './common';

const bootstrap = async () => {
	const logger = new Logger('Startup');
	const app = await NestFactory.create<NestExpressApplication>(AppModule);

	app.disable('x-powered-by');
	const validationPipe = new ValidationPipe({
		whitelist: true,
		stopAtFirstError: true,
		transform: true,
	});

	app.useGlobalPipes(validationPipe);
	app.useGlobalFilters(new GlobalExceptionFilter());
	app.useGlobalInterceptors(new GlobalResponseTransformer());
	app.setGlobalPrefix('api');
	app.enableCors();

	configureSwagger(app);

	await app.listen(appConfig.port);
	logger.log(`App Started on http://localhost:${appConfig.port}/api`);
};

bootstrap();