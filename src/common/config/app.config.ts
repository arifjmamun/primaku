import 'reflect-metadata';
import 'source-map-support/register';

import { Env, parseEnv } from 'atenv';
import { Transform } from 'class-transformer';
import { IsDefined, IsOptional } from 'class-validator';

export enum NodeEnv {
	Local = 'local',
	Dev = 'dev',
	Staging = 'staging',
	Production = 'production',
}

export class AppConfig {
	@Env('NODE_ENV')
	@IsDefined({ message: 'NODE_ENV is required in .env file' })
	nodeEnv: NodeEnv;

	@Env('PORT')
	@IsOptional()
	@Transform(({ value }) => parseInt(value, 10))
	port = 4000;

	@IsDefined({ message: 'JWT access token secret is required in .env file' })
	@Env('JWT_ACCESS_TOKEN_SECRET')
	jwtAccessTokenSecret: string;

	@IsDefined({ message: 'JWT access token `expires in` is required in .env file' })
	@Env('JWT_ACCESS_TOKEN_EXPIRES_IN')
	jwtAccessTokenExpiresIn: string;

	@IsDefined({ message: 'Default user name is required in .env file' })
	@Env('DEFAULT_USER_NAME')
	defaultUsername: string;

	@IsDefined({ message: 'Default user email is required in .env file' })
	@Env('DEFAULT_USER_EMAIL')
	defaultUserEmail: string;

	@IsDefined({ message: 'Default user password is required in .env file' })
	@Env('DEFAULT_USER_PASSWORD')
	defaultUserPassword: string;

	@IsDefined({ message: 'Database host is required in .env file' })
	@Env('DB_HOST')
	dbHost: string;

	@IsDefined({ message: 'Database host is required in .env file' })
	@Env('DB_PORT')
	@Transform(({ value }) => parseInt(value, 10))
	dbPort: number;

	@IsDefined({ message: 'Database username is required in .env file' })
	@Env('DB_USERNAME')
	dbUsername: string;

	@IsDefined({ message: 'Database root password is required in .env file' })
	@Env('DB_ROOT_PASSWORD')
	dbRootPassword: string;

	@IsDefined({ message: 'Database name is required in .env file' })
	@Env('DB_NAME')
	dbName: string;

}

export const appConfig = parseEnv(AppConfig);
