export const PASSWORD_RULE = /^(?!.*\s)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).*$/;
export const PASSWORD_RULE_MESSAGE = 'Password must contain minimum 8 chars, contains uppercase, lowercase, number, and symbol';
export const PASSWORD_VALIDATOR_OPTION = {
	minLength: 8,
	pattern: PASSWORD_RULE,
	message: PASSWORD_RULE_MESSAGE,
};
export const ROLES_KEY = 'roles';