import { NotFoundException, BadRequestException } from '@nestjs/common';

export class UserNotFoundException extends NotFoundException {
	constructor() {
		super({ key: 'USERS.USER_NOT_FOUND' });
	}
}

export class UserNotFoundByIdException extends NotFoundException {
	constructor(userId: number) {
		super({ key: 'USERS.USER_NOT_FOUND_BY_ID', args: { userId } });
	}
}

export class UserNotFoundByEmailException extends NotFoundException {
	constructor(email: string) {
		super({ key: 'USERS.USER_NOT_FOUND_BY_EMAIL', args: { email } });
	}
}

export class UserAlreadyExistsException extends BadRequestException {
	constructor() {
		super({ key: 'USERS.ALREADY_EXISTS' });
	}
}

export class UserInvalidCredentialException extends BadRequestException {
	constructor() {
		super({ key: 'USERS.INVALID_CREDENTIAL' });
	}
}

export class UserInvalidEmailAddressException extends BadRequestException {
	constructor() {
		super({ key: 'USERS.INVALID_EMAIL_ADDRESS' });
	}
}

export class UserInvalidPasswordException extends BadRequestException {
	constructor() {
		super({ key: 'USERS.INVALID_PASSWORD' });
	}
}

export class UserDeactivatedException extends BadRequestException {
	constructor() {
		super({ key: 'USERS.USER_DEACTIVATED' });
	}
}
