import { PickType } from '@nestjs/swagger';

import { User } from '../models/user.model';


export const userDtoAttributes: readonly (keyof User)[] = [
	'id',
	'name',
	'email',
	'role',
	'isActive',
	'createdAt',
	'updatedAt',
];

export class UserDto extends PickType(User, userDtoAttributes) {}
