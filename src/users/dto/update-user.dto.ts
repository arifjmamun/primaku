import { PickType, PartialType } from '@nestjs/swagger';

import { User } from '../models/user.model';

export class UpdateUserDto extends PartialType(PickType(User, ['name', 'role', 'isActive'])) {}
