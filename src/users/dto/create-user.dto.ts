import { ApiProperty, PickType } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

import { Password } from '@app/utils/validators';
import { PASSWORD_VALIDATOR_OPTION } from '../../common/constants';
import { User } from '../models/user.model';

export class AddUserDto extends PickType(User, ['name', 'email', 'role']) {
	@ApiProperty({ required: true })
	@Password(PASSWORD_VALIDATOR_OPTION)
	@Expose()
	password: string;
}
