import { plainToInstance } from 'class-transformer';
import { ConfigService } from '@nestjs/config';
import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Op } from 'sequelize';

import { Hashing } from '@app/utils';
import { AddUserDto, UserDto, userDtoAttributes } from './dto';
import { classTransformOptions, Role } from '../common';
import { User } from './models/user.model';
import { UserNotFoundByIdException } from './users.exceptions';
import { UpdateUserDto } from './dto/update-user.dto';

const userAttributes = userDtoAttributes as string[];

@Injectable()
export class UsersService implements OnModuleInit {
	private logger = new Logger(UsersService.name);

	constructor(
		private readonly configService: ConfigService,
		@InjectModel(User) private readonly userModel: typeof User,
	) {}

	async onModuleInit() {
		await this.seedDefaultUser();
	}

	public async addUser(dto: AddUserDto) {
		dto.password = Hashing.hashPassword(dto.password);
		const user = await this.userModel.create({ ...dto });

		return plainToInstance(UserDto, user, classTransformOptions);
	}

	public async getUserList() {
		const users = await this.userModel.findAll({ attributes: userAttributes });

		return plainToInstance(UserDto, users, classTransformOptions);
	}

	public async getRawUser(identifier: number | string) {
		const user = await this.userModel.findOne({
			where: { [Op.or]: [{ id: identifier }, { email: identifier }] },
		});
		return user;
	}

	public async getUserById(id: number) {
		const user = await this.userModel.findByPk(id, { attributes: userAttributes });
		if (!user) throw new UserNotFoundByIdException(id);
		return plainToInstance(UserDto, user, classTransformOptions);
	}

	public async updateUserById(id: number, dto: UpdateUserDto) {
		const user = await this.userModel.findByPk(id, { attributes: userAttributes });
		if (!user) throw new UserNotFoundByIdException(id);
		await user.update({ ...dto });
		return plainToInstance(UserDto, user, classTransformOptions);
	}

	public async updatePassword(id: number, hashedPassword: string) {
		const user = await this.userModel.findByPk(id);
		if (!user) throw new UserNotFoundByIdException(id);
		await user.update({ password: hashedPassword });
		return plainToInstance(UserDto, user, classTransformOptions);
	}

	public async isUserExistByEmail(email: string) {
		const user = await this.userModel.findOne({ where: { email: email?.toLowerCase().trim() } });
		return !!user;
	}

	public async deleteUserById(id: number) {
		await this.userModel.destroy({ where: { id } });
	}

	public async seedDefaultUser() {
		const email = this.configService.get('DEFAULT_USER_EMAIL');
		const isExist = await this.isUserExistByEmail(email);

		try {
			if (!isExist) {
				await this.userModel.create({
					name: this.configService.get('DEFAULT_USER_NAME'),
					email,
					password: Hashing.hashPassword(this.configService.get('DEFAULT_USER_PASSWORD')),
					role: Role.Admin,
				});

				this.logger.log('Default user seeded');
			}
		} catch (error) {
			this.logger.error(error);
		}
	}
}
