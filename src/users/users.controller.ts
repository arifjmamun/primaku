import { UseGuards, Controller, Get, Post, Body, Delete, Param, Patch } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';

import { Serialize, ApiOkResponse, ApiCreatedResponse, ParsePositiveNumber } from '@app/utils';
import { Role } from '../common';
import { Roles } from './../auth/decorators/roles.decorator';
import { JwtAuthGuard } from './../auth/guards/jwt-auth.guard';
import { UsersService } from './users.service';
import { AddUserDto, UserDto } from './dto';
import { UpdateUserDto } from './dto/update-user.dto';

@ApiTags('Users')
@ApiBearerAuth()
@Serialize()
@Controller('users')
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@ApiOkResponse({ type: UserDto, description: 'Get all users' })
	@Roles(Role.Admin, Role.User)
	@UseGuards(JwtAuthGuard)
	@Get()
	async getAll() {
		return await this.usersService.getUserList();
	}

	@ApiOkResponse({ type: UserDto, description: 'Get a user by ID' })
	@Roles(Role.Admin, Role.User)
	@UseGuards(JwtAuthGuard)
	@Get(':id')
	async getUserById(@Param('id', ParsePositiveNumber) id: number) {
		return await this.usersService.getUserById(id);
	}

	@ApiCreatedResponse({ type: UserDto, description: 'Add a user' })
	@Roles(Role.Admin, Role.User)
	@UseGuards(JwtAuthGuard)
	@Post()
	async addUser(@Body() dto: AddUserDto) {
		return await this.usersService.addUser(dto);
	}

	@ApiOkResponse({ type: UserDto, description: 'Update a user by ID' })
	@Roles(Role.Admin, Role.User)
	@UseGuards(JwtAuthGuard)
	@Patch(':id')
	async updateUserById(@Param('id', ParsePositiveNumber) id: number, @Body() dto: UpdateUserDto) {
		return await this.usersService.updateUserById(id, dto);
	}

	@ApiOkResponse({ description: 'Delete user by ID' })
	@Roles(Role.Admin)
	@UseGuards(JwtAuthGuard)
	@Delete(':id')
	async deleteById(@Param('id', ParsePositiveNumber) id: number) {
		return await this.usersService.deleteUserById(id);
	}
}
