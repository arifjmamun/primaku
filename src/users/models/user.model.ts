import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';
import { IsBoolean, IsEmail, IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { Column, Model, Table } from 'sequelize-typescript';
import { DataTypes } from 'sequelize';

import { Role } from '../../common';

@Table
export class User extends Model {
  @ApiProperty({ required: true })
	@Expose()
  @Column({ autoIncrement: true, autoIncrementIdentity: true, primaryKey: true })
  id: number;

  @ApiProperty({ required: true })
	@IsNotEmpty()
	@IsString()
	@Expose()
  @Column({ allowNull: false })
  name: string;

  @ApiProperty({ required: true })
	@IsNotEmpty()
	@IsEmail()
	@Expose()
	@Transform(({ value }) => value.toLowerCase().trim())
  @Column({ unique: true, allowNull: false })
  email: string;

  @Exclude()
	@IsNotEmpty()
	@IsString()
  @Column({ allowNull: false })
  password: string;

  @ApiProperty({ required: true, enum: Role })
	@Expose()
	@IsEnum(Role)
  @Column({ allowNull: false, type: DataTypes.ENUM, values: Object.values(Role) })
  role: Role;	
  
  @ApiProperty()
	@IsBoolean()
	@Expose()
  @Column({ defaultValue: true })
	isActive: boolean;
}

@Table
export class UserBasic extends Model {
  @Column
  firstName: string;

  @Column
  lastName: string;

  @Column({ defaultValue: true })
  isActive: boolean;
}