import { Role } from '../../common';

export interface DefaultUser {
	name: string;
	email: string;
	password: string;
	role: Role;
	isVerified: boolean;
	avatarURL: string;
}
