import { Role } from "../../common";

export interface ContextUser {
	id: string;
	role: Role;
	email: string;
}
