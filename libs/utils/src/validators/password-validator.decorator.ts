import { applyDecorators } from '@nestjs/common';
import { IsString, IsNotEmpty, Matches, MinLength } from 'class-validator';

type PasswordOption = {
	minLength: number;
	pattern: RegExp;
	message: string;
};

export const Password = ({ minLength, pattern, message }: PasswordOption) => {
	return applyDecorators(IsString(), IsNotEmpty(), MinLength(minLength), Matches(pattern, { message }));
};
