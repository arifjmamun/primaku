import * as bcrypt from 'bcrypt';

export class Hashing {
  public static hashPassword(password: string) {
		const salt = bcrypt.genSaltSync(10);
		return bcrypt.hashSync(password, salt);
	}

  public static verifyPassword(plainTextPassword: string, hashedPassword: string) {
		return bcrypt.compareSync(plainTextPassword, hashedPassword);
	}
}