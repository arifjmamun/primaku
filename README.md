## Description

PrimaKu Backend API services

## Prerequisits
- Ensure that you have a `.env` file in root directory.
- Adjust necessary values by following `.env.sample`

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## To run with Docker Compose

```bash
$ docker-compose up -d

# For force build, use
$ docker-compose --build -d

# To stop the server
$ docker-compose down
```

## Getting started
To explore the APIs navigate to the URL: http://localhost:4000/api
- It includes swagger API documentation, so that you can easily check all the APIs